# MODULE_NAME, MAJOR, MINOR and PATCH must be set for each module
# OBJS must be set for each module

SHELL := /bin/bash

FC := gfortran
RM := @rm -rf -v
AR := ar -q

BINDIR := ./bin
OBJDIR := ./obj
SRCDIR := ./src
LIBDIR := ./lib
INCLUDEDIR := ./include
TESTDIR := ./tests


_OBJS := $(foreach obj, $(OBJS), $(OBJDIR)/$(obj))
_TEST_OBJS := $(foreach obj, $(TEST_OBJS), $(OBJDIR)/$(obj))

LIBS += $(foreach lib, $(PKGS), -L $(LIBDIR)/$(lib)/$(LIBDIR) -l$(lib))

INCLUDES += -I $(INCLUDEDIR)
INCLUDES += $(foreach lib, $(PKGS), -I $(LIBDIR)/$(lib)/$(INCLUDEDIR))


FFLAGS := -Wall -std=f2003 $(LIBS) $(INCLUDES)


STATIC_LIB_FILE := $(LIBDIR)/lib$(MODULE_NAME).a


VPATH := $(VPATH):$(SRCDIR):$(TESTDIR)


.PHONY: compile
compile: | $(PKGS) $(_OBJS)
ifeq ($(findstring main,$(OBJS)),main)
	$(MAKE) $(BINDIR)/$(MODULE_NAME).$(MAJOR).$(MINOR)
endif

$(BINDIR)/$(MODULE_NAME).$(MAJOR).$(MINOR): $(PKGS) $(_OBJS)
	$(FC) -o $@ $^ $(FFLAGS)


.PHONY: libstatic
libstatic: $(STATIC_LIB_FILE)

$(STATIC_LIB_FILE): | $(PKGS) $(_OBJS)
	$(AR) $@ $(_OBJS)


$(OBJDIR)/%.o: %.f90
	$(FC) -c $(FFLAGS) -o $@ $< -J $(INCLUDEDIR)


.PHONY: $(PKGS)
$(PKGS):
	@for lib in $(PKGS); do \
		if [ ! -L $(LIBDIR)/$$lib ]; then \
			make libstatic -C $(LIBDIR)/$$lib; \
		fi \
	done;


.PHONY: test
test: $(TESTDIR)/tests

$(TESTDIR)/tests: compile $(_TEST_OBJS)
	$(FC) -o $@ $(_OBJS) $(_TEST_OBJS) $(TESTDIR)/tests.f90 $(FFLAGS)
	@$@


.PHONY: watch
FILES := `find . -regextype sed -regex ".*[?\.git][a-zA-Z0-9].[f|f90|c]"`
watch: compile
	@printf "Watching following file(s)...\n$(FILES)\n"
	@[ command -v inotifywait >/dev/null 2>&1 ] && exit || true;
	@inotifywait -q -m -e modify $(FILES) | \
	while read -r filename event; do make test; done;


.PHONY: clean
clean:
	$(RM) $(_OBJS) $(_TEST_OBJS)
	$(RM) $(STATIC_LIB_FILE)
	$(RM) $(INCLUDEDIR)/*.mod*
	$(RM) $(BINDIR)/$(MODULE_NAME).$(MAJOR).$(MINOR)
	@for lib in $(PKGS); do \
		make clean -C $(LIBDIR)/$$lib; \
	done;
